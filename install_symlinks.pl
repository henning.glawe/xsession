#!/usr/bin/env perl
use strict;
use warnings;
use File::Spec;
use Cwd;

# configuration variables
my $TARGET_DIR = $ENV{HOME};
my $SOURCE_DIR = Cwd::getcwd() . '/dotfiles';
my $OLD_SUFFIX = 'old';

# global variables for communicating config data and status
my %target_dirs; # map ($dir => 1) for each required and existing target directory
my %link_source; # map ($target => [ $target_dir, $source ]) for each dotfile

sub find_dotfiles {
    die "error calling find" unless open(my $find, '-|', 'find', $SOURCE_DIR, qw#-! -type d -! -name *.swp#);
    while (<$find>) {
        chomp;
        next unless (/^$SOURCE_DIR(((?:\/.*)?)\/[^\/]+)$/);
        my $link_source = $_;
        my $link_target_dir = $TARGET_DIR . $2;
        $target_dirs{$link_target_dir} = 1;
        my $link_target = $TARGET_DIR . $1;
        $link_source{$link_target} = [ $link_target_dir, $link_source ];
    }
    close($find) or die "error closing 'find' pipe";
};

sub create_dirs {
    foreach my $link_target_dir (keys %target_dirs) {
        unless (-d $link_target_dir) {
            if (-e $link_target_dir or -l $link_target_dir) {
                $target_dirs{$link_target_dir} = 0;
                print STDERR "ERROR: name exists, but is not a directory: $link_target_dir\n";
                next;
            }
            if (system('mkdir', '-vp', $link_target_dir) != 0) {
                $target_dirs{$link_target_dir} = 0;
                print STDERR "ERROR: Failed to create directory: $link_target_dir\n";
                next;
            }
        }
    }
};

sub create_symlinks {
    foreach my $link_target (keys %link_source) {
        my ($link_target_dir, $link_source_abs) = @{$link_source{$link_target}};
        unless ($target_dirs{$link_target_dir}) {
            print STDERR "ERROR: bad parent directory of symlink: $link_target\n";
            next;
        };

        my $link_source_rel = File::Spec->abs2rel($link_source_abs, $link_target_dir);
        my $link_source = $link_source_rel;
        if (-l $link_target) {
            my $link_source_present = readlink($link_target);
            next if ($link_source_present eq $link_source);
        }
        if (-e $link_target or -l $link_target) {
            my $backup_file = "${link_target}.${OLD_SUFFIX}";
            print "Do you want to replace $link_target by symlink to $link_source [y/N]?";
            my $answer = readline(STDIN);
            next if ($answer !~ /^\s*[yY]/);
            if (-e $backup_file or -l $backup_file) {
                print "Backup file '$backup_file' does already exist. Overwrite [y/N]?";
                my $answer = readline(STDIN);
                next if ($answer !~ /^\s*[yY]/);
                unlink($backup_file);
            }
            system('mv', '-vf', $link_target, $backup_file);
        }
        system('ln', '-nsvf', $link_source, $link_target);
    }
};


find_dotfiles();
create_dirs();
create_symlinks();
